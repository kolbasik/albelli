IF NOT EXISTS (SELECT TOP (1) 1 FROM [dbo].[Customers])
	BEGIN
		INSERT INTO [dbo].[Customers] ([Name], [Email])
		SELECT 'Person' + cast(t.ID as nvarchar(max)), 'Person' + cast(t.ID as nvarchar(max)) + '@email.com'
		FROM (VALUES (1), (2), (3), (4), (5)) AS t(ID)

		INSERT INTO [dbo].[Orders] ([CustomerId], [Price], [CreatedDate])
		SELECT c.Id, o.Price, o.CreateDate
		FROM [dbo].[Customers] as c
			CROSS APPLY (
				SELECT c.Id * t.ID * RAND(100) [Price], GETUTCDATE() [CreateDate]
				FROM (VALUES (1), (2), (3), (4), (5)) AS t(ID)
			) as o
	END