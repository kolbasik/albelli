CREATE TABLE [dbo].[Orders] (
    [OrderId]     BIGINT          IDENTITY (1, 1) NOT NULL,
    [CustomerId]  BIGINT          NOT NULL,
    [Price]       DECIMAL (18, 2) NOT NULL,
    [CreatedDate] DATETIME        NOT NULL,
    CONSTRAINT [PK_dbo.Orders] PRIMARY KEY CLUSTERED ([OrderId] ASC),
    CONSTRAINT [FK_dbo.Orders_dbo.Customers_CustomerId] FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([Id]) ON DELETE CASCADE
);

GO
CREATE NONCLUSTERED INDEX [IX_dbo.Orders_CustomerId]
    ON [dbo].[Orders]([CustomerId] ASC);
