﻿// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The customers service test.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CustomerOrdersAPI.Tests.Models
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;

    using CustomerOrdersAPI.Infrastructure.Data;
    using CustomerOrdersAPI.Models;

    using Moq;

    using Xunit;

    public class CustomersServiceTest
    {
        private readonly Mock<IDbSet<Customer>> customers;

        private readonly List<Customer> fakes;

        private readonly CustomersService sut;

        private readonly Mock<IUnitOfWork> uow;

        public CustomersServiceTest()
        {
            this.fakes =
                Enumerable.Range(0, 20)
                    .Select(
                        i => new Customer("Name_" + i, i + "_email@gmail.com") { Id = i, Orders = new List<Order>() })
                    .ToList();
            var queryable = this.fakes.AsQueryable();

            this.customers = new Mock<IDbSet<Customer>>();
            this.customers.Setup(x => x.Provider).Returns(queryable.Provider);
            this.customers.Setup(x => x.Expression).Returns(queryable.Expression);
            this.customers.Setup(x => x.ElementType).Returns(queryable.ElementType);
            this.customers.Setup(x => x.GetEnumerator()).Returns(this.fakes.GetEnumerator());

            this.uow = new Mock<IUnitOfWork>();
            this.uow.Setup(x => x.Set<Customer>()).Returns(this.customers.Object);

            this.sut = new CustomersService(this.uow.Object);
        }

        [Fact]
        public void Ctor_Always_CreateAnInstance()
        {
            // assert
            Assert.NotNull(this.sut);
        }

        [Fact]
        public void Dispose_Always_ReleaseUnityOfWork()
        {
            // act
            this.sut.Dispose();

            // assert
            this.uow.Verify(x => x.Dispose(), Times.Once);
        }

        [Theory]
        [InlineData(0, 5)]
        [InlineData(5, 10)]
        [InlineData(10, 3)]
        public void GetCustomers_Always_ReturnLimitedCollection(int skip, int take)
        {
            // arrange
            var expected = this.fakes.Skip(skip).Take(take).ToList();

            // act
            var actual = this.sut.GetCustomers(skip, take);

            // assert
            Assert.Equal(expected, actual);
        }
        
        [Fact]
        public void GetCustomer_NotExists_RaiseNotFoundException()
        {
            // arrange
            var expected = -1;

            // act & assert
            Assert.Throws<NotFoundException>(() => this.sut.GetCustomer(expected));
        }

        [Fact]
        public void GetCustomer_Exists_ReturnOkNegotiatedContentResult()
        {
            // arrange
            var expected = this.fakes.ElementAt(5);
            this.customers.Setup(x => x.Find(expected.Id)).Returns(expected);

            // act
            var actual = this.sut.GetCustomer(expected.Id);

            // assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void CreateCustomer_Always_AddAndCommitNewCustomer()
        {
            // arrange
            var expected = this.fakes.ElementAt(7);
            this.customers.Setup(x => x.Add(expected)).Returns(expected);

            // act
            var actual = this.sut.CreateCustomer(expected);

            // assert
            Assert.Equal(expected, actual);
            this.uow.Verify(x => x.Commit(), Times.Once);
        }

        [Theory]
        [InlineData(5)]
        [InlineData(10)]
        [InlineData(15)]
        public void CreateOrder_Always_AddAndCommitNewOrder(int id)
        {
            // arrange
            var customer = this.fakes.ElementAt(id);
            this.customers.Setup(x => x.Find(customer.Id)).Returns(customer);

            var expected = new Order(25.3m, DateTime.UtcNow);

            // act
            var actual = this.sut.CreateOrder(customer.Id, expected);

            // assert
            Assert.Equal(expected, actual);
            Assert.Contains(expected, customer.Orders);
            this.uow.Verify(x => x.Commit(), Times.Once);
        }
    }
}