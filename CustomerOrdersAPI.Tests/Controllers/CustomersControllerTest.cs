﻿// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The values controller test.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CustomerOrdersAPI.Tests.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Web.Http.Results;

    using CustomerOrdersAPI.Controllers;
    using CustomerOrdersAPI.Models;

    using Moq;

    using Xunit;

    public class CustomersControllerTest
    {
        private readonly Mock<ICustomersService> customersService;

        private readonly CustomersController sut;

        public CustomersControllerTest()
        {
            this.customersService = new Mock<ICustomersService>();
            this.sut = new CustomersController(this.customersService.Object);
        }

        [Fact]
        public void Ctor_Always_CreateAnInstance()
        {
            // assert
            Assert.NotNull(this.sut);
        }

        [Fact]
        public void Dispose_Always_ReleaseCustomersService()
        {
            // act
            this.sut.Dispose();

            // assert
            this.customersService.Verify(x => x.Dispose(), Times.Once);
        }

        [Theory]
        [InlineData(0, 5)]
        [InlineData(5, 10)]
        [InlineData(10, 3)]
        public void GetCustomers_Always_CallGetCustomersFromService(int skip, int take)
        {
            // arrange
            var expected = new Collection<Customer>();
            this.customersService.Setup(x => x.GetCustomers(It.IsIn(skip), It.IsIn(take))).Returns(expected);

            // act
            var result = this.sut.GetCustomers(skip, take);
            var actual = Assert.IsType<OkNegotiatedContentResult<ICollection<Customer>>>(result).Content;

            // assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void GetCustomer_Always_CallGetCustomerFromService()
        {
            // arrange
            var expected = new Customer("James", "james@gmail.com") { Id = 42 };

            this.customersService.Setup(x => x.GetCustomer(expected.Id, true)).Returns(expected);

            // act
            var result = this.sut.GetCustomer(expected.Id);
            var actual = Assert.IsType<OkNegotiatedContentResult<Customer>>(result).Content;

            // assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void PostCustomer_Always_CallCreateCustomerFromService()
        {
            // arrange
            var expected = new Customer("James", "james@gmail.com") { Id = 42 };
            this.customersService.Setup(x => x.CreateCustomer(expected)).Returns(expected);

            // act
            var result = this.sut.PostCustomer(expected);
            var actual = Assert.IsType<CreatedNegotiatedContentResult<Customer>>(result).Content;

            // assert
            Assert.Equal(expected, actual);
            this.customersService.Verify(x => x.CreateCustomer(expected), Times.Once);
        }

        [Fact]
        public void PostOrder_Always_CallCreateCustomerFromService()
        {
            // arrange
            var customerId = 42;
            var expected = new Order(25.3m, DateTime.UtcNow);
            this.customersService.Setup(x => x.CreateOrder(customerId, expected)).Returns(expected);

            // act
            var result = this.sut.PostOrder(customerId, expected);
            var actual = Assert.IsType<CreatedNegotiatedContentResult<Order>>(result).Content;

            // assert
            Assert.Equal(expected, actual);
            this.customersService.Verify(x => x.CreateOrder(customerId, expected), Times.Once);
        }
    }
}