﻿// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The home controller test.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CustomerOrdersAPI.Tests.Controllers
{
    using System.Web.Mvc;

    using CustomerOrdersAPI.Controllers;

    using Microsoft.VisualStudio.TestTools.UnitTesting;

    /// <summary>The home controller test.</summary>
    [TestClass]
    public class HomeControllerTest
    {
        /// <summary>The index.</summary>
        [TestMethod]
        public void Index()
        {
            // Arrange
            var controller = new HomeController();

            // Act
            var result = controller.Index() as ViewResult;

            // Assert
            Assert.IsNotNull(result);
            Assert.AreEqual("Home Page", result.ViewBag.Title);
        }
    }
}