﻿// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The web api config.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CustomerOrdersAPI
{
    using System;
    using System.Net.Http.Formatting;
    using System.Web.Http;
    using System.Web.Http.Filters;

    using CustomerOrdersAPI.Infrastructure.WebApi.Filters;

    using Newtonsoft.Json;

    /// <summary>The web api config.</summary>
    public static class WebApiConfig
    {
        /// <summary> Registers by the specified configure.</summary>
        /// <param name="configure">The configure.</param>
        public static void Register(Action<Action<HttpConfiguration>> configure)
        {
            configure(Register);
        }

        /// <summary>The register.</summary>
        /// <param name="config">The config.</param>
        public static void Register(HttpConfiguration config)
        {
            // Web API configuration and services
            ConfigureFilters(config.Filters);
            ConfigureMediaTypeFormatters(config.Formatters);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });
        }

        /// <summary>The configure media type filters.</summary>
        /// <param name="filters">The filters.</param>
        private static void ConfigureFilters(HttpFilterCollection filters)
        {
            filters.Add(new RequireArgumentsAttribute());
            filters.Add(new RequireValidModelAttribute());
        }

        /// <summary>The configure media type filters.</summary>
        /// <param name="formatters">The formatters.</param>
        private static void ConfigureMediaTypeFormatters(MediaTypeFormatterCollection formatters)
        {
            var jsonMediaTypeFormatter = formatters.JsonFormatter;
            var xmlMediaTypeFormatter = formatters.XmlFormatter;

            jsonMediaTypeFormatter.SerializerSettings.NullValueHandling = NullValueHandling.Ignore;

            formatters.Clear();
            formatters.Add(jsonMediaTypeFormatter); // NOTE: make JSON as default formatter
            formatters.Add(xmlMediaTypeFormatter);
        }
    }
}