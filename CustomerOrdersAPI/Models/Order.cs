// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The order model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CustomerOrdersAPI.Models
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>The order.</summary>
    [DataContract]
    public class Order
    {
        /// <summary>
        ///     Initializes a new instance of the <see cref="Order" /> class.
        /// </summary>
        [Obsolete("Only used by ORM and Formatters.", true)]
        [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
        public Order()
        {
        }

        /// <summary>Initializes a new instance of the <see cref="Order"/> class.</summary>
        /// <param name="price">The price value.</param>
        /// <param name="createdDate">The created date value.</param>
        public Order(decimal price, DateTime createdDate)
        {
            this.Price = price;
            this.CreatedDate = createdDate;
        }

        /// <summary>Gets or sets the order id.</summary>
        /// <value>The order id.</value>
        public long OrderId { get; set; }

        /// <summary>Gets or sets the customer id.</summary>
        /// <value>The customer id.</value>
        public long CustomerId { get; set; }

        /// <summary>Gets or sets the price value.</summary>
        /// <value>The price value.</value>
        [DataMember(Name = "Price", Order = 0, IsRequired = true)]
        public decimal Price { get; set; }

        /// <summary>Gets or sets the created date value.</summary>
        /// <value>The created date value.</value>
        [DataMember(Name = "CreatedDate", Order = 1, IsRequired = true)]
        public DateTime CreatedDate { get; set; }
    }
}