// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The CustomersService interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CustomerOrdersAPI.Models
{
    using System;
    using System.Collections.Generic;

    /// <summary>The CustomersService interface.</summary>
    public interface ICustomersService : IDisposable
    {
        /// <summary>Gets the collection of customers.</summary>
        /// <param name="skip">The skip.</param>
        /// <param name="take">The take.</param>
        /// <returns>The list of customers.</returns>
        ICollection<Customer> GetCustomers(int skip, int take);

        /// <summary>Gets the customer.</summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="includeOrders">if set to <c>true</c> [include orders].</param>
        /// <returns>The customer.</returns>
        Customer GetCustomer(long customerId, bool includeOrders = false);

        /// <summary>Creates a new customer.</summary>
        /// <param name="customer">The customer.</param>
        /// <returns>The new customer.</returns>
        Customer CreateCustomer(Customer customer);

        /// <summary>Creates a new order.</summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="order">The order.</param>
        /// <returns>The new order.</returns>
        Order CreateOrder(long customerId, Order order);
    }
}