﻿// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The customers service.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CustomerOrdersAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;

    using CustomerOrdersAPI.Controllers;
    using CustomerOrdersAPI.Infrastructure.Data;

    /// <summary>The customers service.</summary>
    public class CustomersService : Disposable, ICustomersService
    {
        private readonly IUnitOfWork uow;

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomersController"/> class.
        /// </summary>
        /// <param name="uow">The unit of work.</param>
        public CustomersService(IUnitOfWork uow)
        {
            Guard.ArgumentNotNull(uow, "uow");

            this.uow = uow;
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.uow.Dispose();
            }
        }

        /// <summary>Gets the collection of customers.</summary>
        /// <param name="skip">The skip.</param>
        /// <param name="take">The take.</param>
        /// <returns>The list of customers.</returns>
        public ICollection<Customer> GetCustomers(int skip, int take)
        {
            var customers = this.uow.Set<Customer>().OrderBy(x => x.Id).Skip(skip).Take(take).ToList();
            return customers;
        }

        /// <summary>Gets the customer.</summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="includeOrders">if set to <c>true</c> [include orders].</param>
        /// <returns>The customer.</returns>
        public Customer GetCustomer(long customerId, bool includeOrders = false)
        {
            var customer = this.uow.Set<Customer>().Find(customerId);
            if (customer == null)
            {
                throw new NotFoundException(customerId.ToString(), typeof(Customer).FullName);
            }

            if (includeOrders)
            {
                this.uow.Load(customer, x => x.Orders);
            }

            return customer;
        }

        /// <summary>Creates a new customer.</summary>
        /// <param name="customer">The customer.</param>
        /// <returns>The new customer.</returns>
        public Customer CreateCustomer(Customer customer)
        {
            Guard.ArgumentNotNull(customer, "customer");

            try
            {
                return this.uow.Set<Customer>().Add(customer);
            }
            finally
            {
                this.uow.Commit();
            }
        }

        /// <summary>Creates a new order.</summary>
        /// <param name="customerId">The customer identifier.</param>
        /// <param name="order">The order.</param>
        /// <returns>The new order.</returns>
        public Order CreateOrder(long customerId, Order order)
        {
            Guard.ArgumentNotNull(order, "order");

            var customer = this.GetCustomer(customerId);
            customer.Orders.Add(order);
            this.uow.Commit();

            return order;
        }
    }
}