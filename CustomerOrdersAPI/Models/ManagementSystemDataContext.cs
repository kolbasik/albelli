// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The management system data context.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CustomerOrdersAPI.Models
{
    using System.Data.Entity;

    /// <summary>The management system data context.</summary>
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class ManagementSystemDataContext : DbContext
    {
        /// <summary>
        /// Initializes the <see cref="ManagementSystemDataContext"/> class.
        /// </summary>
        static ManagementSystemDataContext()
        {
            Database.SetInitializer(new NullDatabaseInitializer<ManagementSystemDataContext>());
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ManagementSystemDataContext"/> class.
        /// </summary>
        public ManagementSystemDataContext()
            : base("name=ManagementSystemDataContext")
        {
            this.Configuration.LazyLoadingEnabled = false;
        }

        /// <summary>
        ///     Gets or sets the customers.
        /// </summary>
        public virtual IDbSet<Customer> Customers { get; set; }

        /// <summary>
        ///     Gets or sets the orders.
        /// </summary>
        public virtual IDbSet<Order> Orders { get; set; }
    }
}