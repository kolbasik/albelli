﻿// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The customer model.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CustomerOrdersAPI.Models
{
    using System;
    using System.Collections.Generic;
    using System.Runtime.Serialization;

    /// <summary>The customer model.</summary>
    [DataContract]
    public class Customer
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Customer"/> class.
        /// </summary>
        [Obsolete("Only used by ORM and Formatters.", true)]
        [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
        public Customer()
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="Customer"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="email">The email.</param>
        public Customer(string name, string email)
        {
            this.Name = name;
            this.Email = email;
        }

        /// <summary>Gets or sets the name.</summary>
        /// <value>The name value.</value>
        [DataMember(Name = "Id", Order = 0, IsRequired = true)]
        public long Id { get; set; }

        /// <summary>Gets or sets the name.</summary>
        /// <value>The name value.</value>
        [DataMember(Name = "Name", Order = 1, IsRequired = true)]
        public string Name { get; set; }

        /// <summary>Gets or sets the email.</summary>
        /// <value>The email.</value>
        [DataMember(Name = "Email", Order = 2, IsRequired = true)]
        public string Email { get; set; }

        /// <summary>Gets or sets the orders.</summary>
        /// <value>The orders.</value>
        [DataMember(Name = "Orders", Order = 3, IsRequired = false, EmitDefaultValue = true)]
        public ICollection<Order> Orders { get; set; }
    }
}