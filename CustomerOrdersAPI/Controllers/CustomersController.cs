﻿// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The customers controller.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CustomerOrdersAPI.Controllers
{
    using System;
    using System.Web.Http;

    using CustomerOrdersAPI.Infrastructure.Data;
    using CustomerOrdersAPI.Models;

    /// <summary>The customers controller.</summary>
    [RoutePrefix("V1/Customers")]
    public class CustomersController : ApiController
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="CustomersController"/> class.
        /// </summary>
        [Obsolete("Only used by WebApi.", true)]
        [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
        public CustomersController()
            : this(new CustomersService(new UnitOfWork(new ManagementSystemDataContext())))
        {
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="CustomersController"/> class.
        /// </summary>
        /// <param name="customersService">The unit of work.</param>
        public CustomersController(ICustomersService customersService)
        {
            Guard.ArgumentNotNull(customersService, "customersService");

            this.CustomersService = customersService;
        }

        /// <summary>
        /// Gets the customers service.
        /// </summary>
        /// <value>
        /// The customers service.
        /// </value>
        public ICustomersService CustomersService { get; private set; }

        /// <summary>
        /// Gets the customer location.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The customer location.</returns>
        public static Uri GetCustomerLocation(long id)
        {
            return new Uri("/V1/Customers/" + id, UriKind.Relative);
        }

        /// <summary>Gets the customers.</summary>
        /// <param name="skip">The skip.</param>
        /// <param name="take">The take.</param>
        /// <returns>The list of customers.</returns>
        [HttpGet, Route("")]
        public IHttpActionResult GetCustomers(
            [FromUri(Name = "$skip")] int skip = 0, 
            [FromUri(Name = "$take")] int take = 50)
        {
            var customers = this.CustomersService.GetCustomers(skip, take);

            return this.Ok(customers);
        }

        /// <summary>
        /// Gets the customer.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns>The customer.</returns>
        [HttpGet, Route("{id}")]
        public IHttpActionResult GetCustomer(long id)
        {
            var customer = this.CustomersService.GetCustomer(id, includeOrders: true);

            return this.Ok(customer);
        }

        /// <summary>
        /// Creates a new customer.
        /// </summary>
        /// <param name="customer">The customer.</param>
        /// <returns>The new customer.</returns>
        [HttpPost, Route("")]
        public IHttpActionResult PostCustomer([FromBody] Customer customer)
        {
            Guard.ArgumentNotNull(customer, "customer");

            customer = this.CustomersService.CreateCustomer(customer);

            var location = GetCustomerLocation(customer.Id);
            return this.Created(location, customer);
        }

        /// <summary>Creates a new order.</summary>
        /// <param name="id">The identifier.</param>
        /// <param name="order">The order.</param>
        /// <returns>The new order.</returns>
        [HttpPost, Route("{id}")]
        public IHttpActionResult PostOrder(long id, [FromBody] Order order)
        {
            Guard.ArgumentNotNull(order, "order");

            order = this.CustomersService.CreateOrder(id, order);

            var location = GetCustomerLocation(id);
            return this.Created(location, order);
        }

        /// <summary>
        /// Releases the unmanaged resources that are used by the object and, optionally, releases the managed resources.
        /// </summary>
        /// <param name="disposing">true to release both managed and unmanaged resources; false to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.CustomersService.Dispose();
            }

            base.Dispose(disposing);
        }
    }
}