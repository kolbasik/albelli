﻿// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The not found entity exception.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CustomerOrdersAPI.Infrastructure.Data
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>The not found entity exception.</summary>
    [Serializable]
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class NotFoundException : ApplicationException, ISerializable
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotFoundException" /> class.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="resource">The resource.</param>
        public NotFoundException(string id, string resource)
            : base(string.Format("Could not find the '{1}' resource with id: '{0}'.", id, resource))
        {
            this.Id = id;
            this.Resource = resource;
        }

        /// <summary>Gets the identifier.</summary>
        /// <value>The identifier.</value>
        public string Id { get; private set; }

        /// <summary>Gets the resource.</summary>
        /// <value>The resource.</value>
        public string Resource { get; private set; }

        /// <summary>
        /// Initializes a new instance of the <see cref="NotFoundException"/> class.
        /// </summary>
        /// <param name="info">The object that holds the serialized object data.</param>
        /// <param name="context">The contextual information about the source or destination.</param>
        protected NotFoundException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
            Guard.ArgumentNotNull(info, "info");
            Guard.ArgumentNotNull(context, "context");

            this.Id = info.GetString("Id");
            this.Resource = info.GetString("Resource");
        }

        /// <summary>
        /// When overridden in a derived class, sets the <see cref="T:System.Runtime.Serialization.SerializationInfo" /> with information about the exception.
        /// </summary>
        /// <param name="info">The <see cref="T:System.Runtime.Serialization.SerializationInfo" /> that holds the serialized object data about the exception being thrown.</param>
        /// <param name="context">The <see cref="T:System.Runtime.Serialization.StreamingContext" /> that contains contextual information about the source or destination.</param>
        public override void GetObjectData(SerializationInfo info, StreamingContext context)
        {
            Guard.ArgumentNotNull(info, "info");
            Guard.ArgumentNotNull(context, "context");

            base.GetObjectData(info, context);

            info.AddValue("Id", this.Id);
            info.AddValue("Resource", this.Resource);
        }
    }
}