// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The UnitOfWork interface.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CustomerOrdersAPI.Infrastructure.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq.Expressions;

    /// <summary>The UnitOfWork interface.</summary>
    public interface IUnitOfWork : IDisposable
    {
        /// <summary>The set of entities.</summary>
        /// <typeparam name="T">The entity type</typeparam>
        /// <returns>The <see cref="IDbSet"/>.</returns>
        IDbSet<T> Set<T>() where T : class;

        /// <summary>
        /// The load the related entities.
        /// </summary>
        /// <typeparam name="TEntity">The entity type</typeparam>
        /// <typeparam name="TRelated">The type of the related entity.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <param name="navigationProperty">The navigation property.</param>
        void Load<TEntity, TRelated>(TEntity entity, Expression<Func<TEntity, ICollection<TRelated>>> navigationProperty)
            where TEntity : class
            where TRelated : class;

        /// <summary>The commit.</summary>
        void Commit();
    }
}