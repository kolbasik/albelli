// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The unit of work.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CustomerOrdersAPI.Infrastructure.Data
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq.Expressions;

    /// <summary>The unit of work.</summary>
    [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
    public class UnitOfWork : Disposable, IUnitOfWork
    {
        /// <summary>The context.</summary>
        private readonly DbContext context;

        /// <summary>Initializes a new instance of the <see cref="UnitOfWork"/> class.</summary>
        /// <param name="context">The context.</param>
        public UnitOfWork(DbContext context)
        {
            Guard.ArgumentNotNull(context, "context");

            this.context = context;
        }

        /// <summary>
        ///     The set of entities.
        /// </summary>
        /// <typeparam name="T">The entity type</typeparam>
        /// <returns>
        ///     The <see cref="IDbSet" />.
        /// </returns>
        public IDbSet<T> Set<T>() where T : class
        {
            return this.context.Set<T>();
        }

        /// <summary>
        /// The load the related entities.
        /// </summary>
        /// <typeparam name="TEntity">The entity type</typeparam>
        /// <typeparam name="TRelated">The type of the related entity.</typeparam>
        /// <param name="entity">The entity.</param>
        /// <param name="navigationProperty">The navigation property.</param>
        public void Load<TEntity, TRelated>(TEntity entity, Expression<Func<TEntity, ICollection<TRelated>>> navigationProperty)
            where TEntity : class
            where TRelated : class
        {
            this.context.Entry<TEntity>(entity).Collection(navigationProperty).Load();
        }

        /// <summary>
        ///     The commit.
        /// </summary>
        public void Commit()
        {
            this.context.SaveChanges();
        }

        /// <summary>
        /// Releases unmanaged and - optionally - managed resources.
        /// </summary>
        /// <param name="disposing"><c>true</c> to release both managed and unmanaged resources; <c>false</c> to release only unmanaged resources.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                this.context.Dispose();
            }
        }
    }
}