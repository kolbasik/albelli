﻿// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The guard.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace System
{
    using System.Diagnostics;
    using System.Diagnostics.CodeAnalysis;

    /// <summary>The guard.</summary>
    [ExcludeFromCodeCoverage]
    [DebuggerStepThrough, DebuggerNonUserCode]
    public static class Guard
    {
        /// <summary>The argument not null.</summary>
        /// <param name="argumentValue">The argument value.</param>
        /// <param name="argumentName">The argument name.</param>
        /// <exception cref="ArgumentNullException"></exception>
        public static void ArgumentNotNull(object argumentValue, string argumentName)
        {
            if (argumentValue == null)
            {
                throw new ArgumentNullException(argumentName);
            }
        }
    }
}