// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The validate model attribute.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CustomerOrdersAPI.Infrastructure.WebApi.Filters
{
    using System.Net;
    using System.Net.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;

    /// <summary>The validate model attribute.</summary>
    public sealed class RequireValidModelAttribute : ActionFilterAttribute
    {
        /// <summary>The status code.</summary>
        private const HttpStatusCode StatusCode = HttpStatusCode.BadRequest;

        /// <summary>The on action executing.</summary>
        /// <param name="actionContext">The action context.</param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var modelState = actionContext.ModelState;
            if (!modelState.IsValid)
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(StatusCode, modelState);
            }
        }
    }
}