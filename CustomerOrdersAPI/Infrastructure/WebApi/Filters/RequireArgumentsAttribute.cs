// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The require arguments attribute.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CustomerOrdersAPI.Infrastructure.WebApi.Filters
{
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http.Controllers;
    using System.Web.Http.Filters;

    /// <summary>The require arguments attribute.</summary>
    public sealed class RequireArgumentsAttribute : ActionFilterAttribute
    {
        /// <summary>The status code.</summary>
        private const HttpStatusCode StatusCode = HttpStatusCode.BadRequest;

        /// <summary>The names.</summary>
        private readonly string[] names;

        /// <summary>Initializes a new instance of the <see cref="RequireArgumentsAttribute"/> class.</summary>
        /// <param name="names">The names.</param>
        public RequireArgumentsAttribute(params string[] names)
        {
            this.names = names;
        }

        /// <summary>The on action executing.</summary>
        /// <param name="actionContext">The action context.</param>
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var arguments = actionContext.ActionArguments.AsEnumerable();
            if (this.names.Length > 0)
            {
                arguments = arguments.Where(kvp => this.names.Contains(kvp.Key));
            }

            var required = arguments.Where(x => x.Value == null).Select(x => x.Key).ToArray();
            if (required.Length > 0)
            {
                var message = string.Format("The list of required arguments: {0}.", string.Join(", ", required));
                actionContext.Response = actionContext.Request.CreateErrorResponse(StatusCode, message);
            }
        }
    }
}