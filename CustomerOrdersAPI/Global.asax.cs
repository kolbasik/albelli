﻿// --------------------------------------------------------------------------------------------------------------------
// <summary>
//   The web api application.
// </summary>
// --------------------------------------------------------------------------------------------------------------------

namespace CustomerOrdersAPI
{
    using System.Web;
    using System.Web.Http;
    using System.Web.Mvc;

    /// <summary>The web api application.</summary>
    public class WebApiApplication : HttpApplication
    {
        /// <summary>The application_ start.</summary>
        [System.Diagnostics.CodeAnalysis.ExcludeFromCodeCoverage]
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            WebApiConfig.Register(GlobalConfiguration.Configure);
            MvcConfig.Register();
        }
    }
}